import Vue from 'vue'
import Vuex from 'vuex'
import pkmnList from './modules/pkmnList';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    pkmnList
  }
})


// What do i want to do
/*

Welcome to the world of pokemon!
  - what is your name?
  - choose gender 

story about living with pokemon

Here, we do it a little differently. You can choose your first pokemon to be any.
The rest will be chosen at random
Are you ready to choose your 6 pokemon?

- get a list of all pokemon and its ID 
- allow user to select first pokemon that they want 
- allow user to remove and re-select pokemon as needed
- lock in selection
- confirm, then choose the 5 remaining at random

*/